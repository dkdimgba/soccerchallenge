<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
     <title>Nigeria Soccer Fans Challenge | NSFC</title> 
       <meta name="keywords" content="HTML5 Design For NSFC" />
    <meta name="description" content="Nigeria Soccer Fans Challenge">
    <meta name="author" content="westwebtech.com">     

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link href="css/style.css" rel="stylesheet" media="screen">

    <!-- Skins Theme -->
    <link href="#" rel="stylesheet" media="screen" class="skin">

   <?php include 'php/includes/header.php';
?>
        <!-- End Header-->


        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="index.html">Home</a> / Contact Us
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1>Contact Us
                            <span class="subtitle-section">
                                Email Us Now
                                <span class="left"></span>
                                <span class="right"></span>
                            </span>
                            <span class="line-title"></span>
                        </h1>
                    </div>
                    <!-- End Title--> 

                    <!-- Search--> 
                    <div class="col-md-3">
                        <form class="search" action="#" method="Post">
                            <div class="input-group">
                                <input class="form-control" placeholder="Search..." name="email"  type="email" included="included">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" name="subscribe" >Go!</button>
                                </span>
                            </div>
                        </form>  
                    </div>
                    <!-- End Search--> 
                </div>
                <!-- End Title -Search --> 
              
            </div>
        </section>   
        <!-- End Title Section --> 

        <!-- Google Map --> 
        <div id="map"></div>
        <!-- End Google Map --> 

        <!-- Contact Us -->
        <section class="paddings">
            <div class="container">
                <div class="row">

                    <!-- Sidebars -->
                    <div class="col-md-4 sidebars">

                        <aside>
                            <h4>The Office</h4>
                            <address>
                              <strong>NSFC, Inc.</strong><br>
                              <i class="fa fa-map-marker"></i><strong>Address: </strong> fa795 Folsom Ave, Suite 600<br>
                              <i class="fa fa-plane"></i><strong>City: </strong>San Francisco, CA 94107<br>
                              <i class="fa fa-phone"></i> <abbr title="Phone">P:</abbr> (123) 456-7890
                            </address>

                            <address>
                              <strong>NSFC Emails</strong><br>
                              <i class="fa fa-envelope"></i><strong>Email:</strong><a href="mailto:#"> sales@NSFC.com</a><br>
                              <i class="fa fa-envelope"></i><strong>Email:</strong><a href="mailto:#"> support@NSFC.com</a>
                            </address>
                        </aside>

                        <hr class="tall">

                         <aside>
                            <h4>Recent flickr</h4>
                            <ul id="flickr-aside" class="thumbs"></ul>
                        </aside>
               
                    </div>
                    <!-- End Sidebars -->

                    <!-- Content Right -->
                    <div class="col-md-8">
                        <h3>Contact Info</h3>
                        <p class="lead">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </p>

                        <hr class="tall">
                        
                        <!-- Tabs -->
                        <div class="tabs">
                            <!-- Tab nav -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#contactUs" data-toggle="tab">
                                    <i class="fa fa-envelope"></i> Contact Us</a>
                                </li>
                                <li class=""><a href="#support" data-toggle="tab">
                                    <i class="fa fa-bullhorn"></i>Suport</a>
                                </li>
                            </ul>
                            <!-- End Tab nav -->

                            <!-- Tab Content -->
                            <div class="tab-content">
                                <!-- Tab item -->
                                <div class="tab-pane active" id="contactUs">
                                    
                                    <h4>Contact Us</h4>

                                    <!-- Form Contact -->
                                    <form class="form-contact" action="php/send-mail-contact.php">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>Your name *</label>
                                                    <input type="text"  included="included" value="" maxlength="100" class="form-control" name="Name" id="name">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Your email address *</label>
                                                    <input type="email"  included="included" value="" maxlength="100" class="form-control" name="Email" id="email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>Comment *</label>
                                                    <textarea maxlength="5000" rows="10" class="form-control" name="message"  style="height: 138px;" included="included" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="submit" value="Send Message" class="btn btn-lg btn-primary">
                                            </div>
                                        </div>
                                    </form>
                                    <!-- End Form Contact --> 
                                    <div class="result"></div>
                                </div>
                                <!-- End Tab item -->

                                <!-- Tab item -->
                                <div class="tab-pane" id="support">
                                    
                                    <h4>Support</h4>

                                    <!-- Form Contact -->
                                    <form class="form-contact" action="php/send-mail-support.php">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>Your name *</label>
                                                    <input type="text"  included="included" value="" maxlength="100" class="form-control" name="Name">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Your email address *</label>
                                                    <input type="email"  included="included" value="" maxlength="100" class="form-control" name="Email">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>Subject</label>
                                                    <input type="text"  included="included" value="" class="form-control" name="Subject">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Phone</label>
                                                    <input type="number"  included="included" value="" class="form-control" name="Phone">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Problem</label>
                                                <select class="form-control" name="Problem">
                                                  <option>Licence</option>
                                                  <option>Network</option>
                                                  <option>Software</option>                                                  
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>Comment *</label>
                                                    <textarea maxlength="5000" rows="10" class="form-control" name="message"  style="height: 138px;" included="included" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="submit" value="Send Message" class="btn btn-lg btn-primary">
                                            </div>
                                        </div>
                                    </form>
                                    <!-- End Form Contact --> 
                                    <div class="result"></div>
                                </div>
                                <!-- End Tab item -->

                            </div>
                            <!-- End Tab Content -->
                        </div>  
                        <!-- End Tabs -->                     

                    </div>
                    <!-- End Content Right -->


                </div>
            </div>
            <!-- End Container-->
        </section>
        <!-- End Contact Us-->
   

        
    
        <!-- Clients -->
        <section class="paddings clients">
            <div class="container">
               <div class="row">   

                    <!-- title-downloads -->             
                    <h1 class="title-downloads">
                        <span class="logo-clients">Over</span>  
                        <span class="responsive-numbers">
                            <span>2</span>
                            ,
                            <span>3</span>
                            <span>8</span>
                            <span>9</span>
                            ,
                            <span>5</span>
                            <span>1</span>
                            <span>8</span>
                        </span>
                         <span class="logo-clients">Nigerian Soccer Fans</span>
                        
                    </h1>  
                    <!-- End title-downloads -->     
                    
                    <!-- subtitle-downloads --> 
                    <div class="subtitle-downloads">
                        <div class="line"></div>
                        <h4>Official <i class="fa fa-star"></i> Sponsors</h4>
                    </div> 
                    <!-- End subtitle-downloads --> 

                    <!-- Image Clients Downloads --> 
                    <ul class="image-clients-downloads">
                        <li><img src="img/clients-downloads/10.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/1.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/2.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/3.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/4.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/5.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/6.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/7.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/8.jpg" alt=""></li>
                         <li><img src="img/clients-downloads/11.jpg" alt=""></li>
                    </ul>
                    <!-- End Image Clients Downloads --> 
               </div>                
            </div>
        </section>
        <!-- End Clients -->

<!-- Sponsors -->
        <section class="sponsors">
            <div class="overflow-sponsors">
                <div class="container paddings">

                   <h2>You are<span> 65 </span>questions away from becoming a millionaire</h2>
                                   
            </div>
                      
                             
                </div>
            </div>
        </section>
        <!-- End Sponsors -->
       
        

        <!-- footer bottom-->
        <footer class="footer-bottom">
            <div class="container">
               <div class="row">   
                                                                  
                    <!-- Nav-->
                    <div class="col-md-8">
                        <div class="logo-footer">
                            <h2><span>N</span>SFC<span>.</span></h2>
                        </div>
                        <!-- Menu-->
                        <ul class="menu-footer">
                            <li><a href="index.php">Home</a> </li>
                            <li><a href="about.php">How It Works</a> </li>
                         <li><a href="photos.php">Photos</a></li>
                            <li><a href="tv-schedule.php">TV Schedule</a></li>
                            <li><a href="faq.php">FAQ</a></li> 
                           
                            <li><a href="terms-conditions.php">Terms and Conditions</a></li>                                                     
                           
                           
                        </ul>
                        <!-- End Menu-->

                        <!-- coopring-->
                       <div class="row coopring">
                           <div class="col-md-8">
                               <p>&copy; 2015 NSFC . All Rights Reserved.</p>
                           </div>
                       </div>    
                       <!-- End coopring-->  

                    </div>
                    <!-- End Nav-->

                    <!-- Social-->
                    <div class="col-md-4">
                        <!-- Menu-->
                        <ul class="social">
                            <li data-toggle="tooltip" title data-original-title="Facebook">
                                <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Twitter">
                                <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Youtube">
                                <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>                     
                        </ul>
                        <!-- End Menu-->
                    </div>
                    <!-- End Social-->

               </div> 
                    
            </div>
        </footer>      
        <!-- End footer bottom-->

    </div>
    <!-- ======================= JQuery libs =========================== -->
    <!-- Always latest version of jQuery-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <!-- jQuery local-->    
    <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>    
    <!--Nav-->
    <script type="text/javascript" src="js/nav/tinynav.js"></script> 
    <script type="text/javascript" src="js/nav/superfish.js"></script>  
    <script type="text/javascript" src="js/nav/hoverIntent.js"></script>  
    <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>                                           
    <!--Totop-->
    <script type="text/javascript" src="js/totop/jquery.ui.totop.js" ></script>  
    <!--Slide-->
    <script type="text/javascript" src="js/slide/camera.js" ></script>      
    <script type='text/javascript' src='js/slide/jquery.easing.1.3.min.js'></script>  
    <!--Ligbox--> 
    <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script> 
    <!-- carousel.js-->
    <script src="js/carousel/carousel.js"></script>  
    <!-- Twitter Feed-->
    <script src="js/twitter/jquery.tweet.js"></script> 
    <!-- flickr Feed-->
    <script src="js/flickr/jflickrfeed.min.js"></script>  
    <!--Scroll-->   
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Nicescroll -->
    <script src="js/scrollbar/jquery.nicescroll.js"></script>
    <!-- Maps -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/maps/gmap3.js"></script>
    <!-- Filter -->
    <script src="js/filters/jquery.isotope.js" type="text/javascript"></script> 
    <!--Theme Options-->
    <script type="text/javascript" src="js/theme-options/theme-options.js"></script>
    <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>                                
    <!-- Bootstrap.js-->
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <!--MAIN FUNCTIONS-->
    <script type="text/javascript" src="js/main.js"></script>
    <!-- ======================= End JQuery libs =========================== -->
    
    </body>
</html>