-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 28, 2017 at 12:54 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nsf`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE IF NOT EXISTS `access_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `module_name`) VALUES
(1, 'Manage Staff'),
(2, 'Manage Questions'),
(3, 'Manage Contestants Records'),
(4, 'Manage Scratch Card System'),
(5, 'Manage News'),
(7, 'Add New Staff');

-- --------------------------------------------------------

--
-- Table structure for table `access_rights`
--

CREATE TABLE IF NOT EXISTS `access_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_by` int(11) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `access_rights`
--

INSERT INTO `access_rights` (`id`, `access_id`, `user_id`, `user_by`, `post_time`) VALUES
(13, 9, 15, 12, '2013-03-08 23:53:26'),
(12, 14, 15, 12, '2013-03-08 23:53:22'),
(11, 18, 15, 12, '2013-03-08 23:51:06'),
(10, 19, 15, 12, '2013-03-08 23:51:02'),
(9, 20, 15, 12, '2013-03-08 23:29:15'),
(14, 6, 15, 12, '2013-03-08 23:53:30'),
(27, 1, 12, 12, '2013-03-09 00:09:49'),
(28, 2, 12, 12, '2013-03-09 00:09:55'),
(42, 4, 12, 12, '2014-11-04 11:29:32'),
(30, 5, 12, 12, '2013-03-09 00:10:05'),
(31, 6, 12, 12, '2013-03-09 00:10:09'),
(32, 7, 12, 12, '2013-03-09 00:10:18'),
(33, 3, 12, 12, '2013-03-09 00:10:35'),
(35, 11, 4, 12, '2013-03-14 11:52:02'),
(41, 3, 13, 12, '2014-07-12 12:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `answered`
--

CREATE TABLE IF NOT EXISTS `answered` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `exam_id` varchar(50) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` varchar(11) NOT NULL,
  `num` int(11) NOT NULL,
  `correct` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `answered`
--

INSERT INTO `answered` (`id`, `user_id`, `exam_id`, `question_id`, `option_id`, `num`, `correct`, `round`, `post_time`) VALUES
(1, 1, '', 38, 'F', 3, 0, 1, '2015-02-10 14:41:42'),
(2, 1, '', 42, 'F', 4, 0, 1, '2015-02-10 14:42:02'),
(3, 1, '', 51, '', 5, 0, 1, '2015-02-10 14:42:16'),
(4, 1, '', 48, 'F', 5, 0, 1, '2015-03-23 18:16:09'),
(5, 1, '', 49, 'F', 5, 0, 1, '2015-03-23 18:16:46');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(10000) NOT NULL,
  `num` varchar(1) NOT NULL,
  `correct` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=126 ;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `answer`, `num`, `correct`) VALUES
(33, 38, 'Real Madrid ', 'A', 1),
(32, 37, 'Unity of European football association', 'C', 0),
(31, 37, 'United European football association', 'B', 0),
(30, 37, 'Union of European Football associations ', 'A', 1),
(29, 36, 'Arsenal', 'C', 1),
(28, 36, 'Chelsea FC ', 'B', 0),
(27, 36, 'Kano Pillars', 'A', 0),
(26, 35, 'The Gunners', 'C', 1),
(25, 35, 'The Red Devils', 'B', 0),
(24, 35, 'The blues', 'A', 0),
(23, 34, 'The Yellows', 'C', 0),
(22, 34, 'The White', 'B', 0),
(17, 33, 'Brazil ', 'A', 0),
(18, 33, 'France', 'B', 1),
(19, 33, 'Germany', 'C', 0),
(21, 34, 'The Blues', 'A', 1),
(34, 38, 'Bayern Munich ', 'B', 0),
(35, 38, 'Manchester United', 'C', 0),
(36, 39, 'Egypt ', 'A', 0),
(37, 39, 'Algeria', 'B', 0),
(38, 39, 'Zaire', 'C', 1),
(39, 40, '1902', 'A', 0),
(40, 40, '1903', 'B', 0),
(41, 40, '1904', 'C', 1),
(42, 41, 'England ', 'A', 0),
(43, 41, 'Spain', 'B', 0),
(44, 41, 'Uruguay', 'C', 1),
(45, 42, 'Nigerian Football federation ', 'A', 1),
(46, 42, 'Nigeria football fighters', 'B', 0),
(47, 42, 'Nigeria football farmers', 'C', 0),
(48, 43, '1993', 'A', 0),
(49, 43, '1992', 'B', 0),
(50, 43, '1991', 'C', 1),
(51, 44, 'English, French, and German', 'A', 1),
(52, 44, 'English, Spanish and German', 'B', 0),
(53, 44, 'English, French and pidgin', 'C', 0),
(54, 45, 'Algeria', 'A', 0),
(55, 45, 'Nigeria', 'B', 0),
(56, 45, 'Egypt', 'C', 1),
(57, 46, 'Real Madrid', 'A', 1),
(58, 46, 'Manchester United', 'B', 0),
(59, 46, 'Chelsea Club', 'C', 0),
(60, 47, '1956', 'A', 0),
(61, 47, '1957', 'B', 1),
(62, 47, '1958', 'C', 0),
(63, 48, 'Brazil', 'A', 0),
(64, 48, 'Germany ', 'B', 1),
(65, 48, 'France', 'C', 0),
(66, 49, '13', 'A', 0),
(67, 49, '12', 'B', 1),
(68, 49, '11', 'C', 0),
(69, 50, 'Arsenal FC', 'A', 0),
(70, 50, 'Kwara united', 'B', 0),
(71, 50, 'Sheffield FC', 'C', 1),
(72, 51, 'Kenya', 'A', 0),
(73, 51, 'Egypt', 'B', 0),
(74, 51, 'Sudan', 'C', 1),
(75, 52, '1960', 'A', 0),
(76, 52, '1961', 'B', 0),
(77, 52, '1962', 'C', 1),
(78, 53, 'Isah Ayatu', 'A', 0),
(79, 53, 'Abdelaziz Abdullahi Salem', 'B', 0),
(80, 53, 'Issa Hayatou', 'C', 1),
(81, 54, 'World war II', 'A', 1),
(82, 54, 'Insufficient fundS', 'B', 0),
(83, 54, 'No host', 'C', 0),
(84, 55, 'Peter Odewingie ', 'A', 0),
(85, 55, 'Seydou Doumbia', 'B', 1),
(86, 55, 'Didier Drogba', 'C', 0),
(87, 56, 'Fabien Barthez ', 'A', 0),
(88, 56, 'Oliver kahn', 'B', 1),
(89, 56, 'Gianluigi Buffon ', 'C', 0),
(90, 57, 'Franz Beckanbauer and Mario Zagallo', 'A', 1),
(91, 57, 'Mario Zagallo and Michel Platini', 'B', 0),
(92, 57, 'Michel Platini and Franz Beckanbauer', 'C', 0),
(93, 58, 'Japan', 'A', 0),
(94, 58, 'South Korea', 'B', 0),
(95, 58, 'Indonesia (Dutch East Indies)', 'C', 1),
(96, 59, 'Ronaldo De Lima', 'A', 0),
(97, 59, 'Gabriel Batistuta', 'B', 1),
(98, 59, 'Miroslav Klose', 'C', 0),
(99, 60, 'Spain', 'A', 0),
(100, 60, 'Sweden', 'B', 0),
(101, 60, 'Turkey', 'C', 1),
(102, 61, 'Lionel Messi', 'A', 0),
(103, 61, 'Diego Maradona', 'B', 0),
(104, 61, 'Gabriel Batistuta', 'C', 1),
(105, 62, 'Luis Monti', 'A', 1),
(106, 62, 'Luiz Monteno', 'B', 0),
(107, 62, 'Lizarazu Dino', 'C', 0),
(108, 63, '1998', 'A', 0),
(109, 63, '1962', 'B', 0),
(110, 63, '1966', 'C', 1),
(111, 64, 'FC Poto', 'A', 0),
(112, 64, 'Valencia', 'B', 0),
(113, 64, 'Benfica', 'C', 1),
(114, 65, 'Paolo Di Canio', 'A', 1),
(115, 65, 'Paolo De Piero', 'B', 0),
(116, 65, 'Paulo Di cano', 'C', 0),
(117, 66, 'Brazuka', 'A', 0),
(118, 66, 'Brazuca', 'B', 1),
(119, 66, 'Brazilia', 'C', 0),
(120, 67, 'QPR', 'A', 1),
(121, 67, 'Reading', 'B', 0),
(122, 67, 'Wigan', 'C', 0),
(123, 68, 'answer a', 'A', 0),
(124, 68, 'answer b', 'B', 1),
(125, 68, 'answer c', 'C', 0);

-- --------------------------------------------------------

--
-- Table structure for table `attempts`
--

CREATE TABLE IF NOT EXISTS `attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(100) NOT NULL,
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `times_active` int(11) NOT NULL,
  `times` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `attempts`
--

INSERT INTO `attempts` (`id`, `ip`, `last_time`, `times_active`, `times`) VALUES
(1, '::1', '2013-01-21 16:08:59', 0, 1),
(2, '127.0.0.1', '2013-02-09 15:13:04', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cat`
--

CREATE TABLE IF NOT EXISTS `cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cat`
--

INSERT INTO `cat` (`id`, `cat`) VALUES
(1, 'Politics'),
(2, 'Education'),
(3, 'Health'),
(4, 'Economy'),
(5, 'Developing Story'),
(6, 'Security'),
(7, 'empty'),
(8, 'empty');

-- --------------------------------------------------------

--
-- Table structure for table `clubs`
--

CREATE TABLE IF NOT EXISTS `clubs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_name` varchar(50) NOT NULL,
  `country` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `clubs`
--

INSERT INTO `clubs` (`id`, `club_name`, `country`) VALUES
(1, 'Manchester united', 'England'),
(2, 'Real Madrid', 'Spain'),
(3, 'FC Barcelona', 'Spain'),
(4, 'Chelsea FC', 'England'),
(5, 'Liverpool FC', 'England'),
(6, 'Arsenal FC', 'England'),
(7, 'Rangers', 'Nigeria'),
(8, 'Kano Pillars', 'Nigeria'),
(9, 'Shooting stars  ', 'Nigeria');

-- --------------------------------------------------------

--
-- Table structure for table `contestants`
--

CREATE TABLE IF NOT EXISTS `contestants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `club_id` int(11) NOT NULL,
  `state` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `completed_test` int(11) NOT NULL,
  `is_paid` int(11) NOT NULL,
  `pay_type` varchar(2) NOT NULL,
  `pay_code` varchar(20) NOT NULL,
  `question_now` int(11) NOT NULL,
  `round_now` int(11) NOT NULL,
  `test_score` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contestants`
--

INSERT INTO `contestants` (`id`, `fullname`, `phone`, `club_id`, `state`, `email`, `ip`, `post_time`, `completed_test`, `is_paid`, `pay_type`, `pay_code`, `question_now`, `round_now`, `test_score`) VALUES
(1, 'Ugochukwu Daniel', '08039400379', 1, 'Abia', 'ugdaniel@gmail.com', '::1', '2015-02-10 14:40:26', 0, 1, 'sc', '1130608587', 5, 1, 0),
(2, 'kkk', '999999', 1, 'Abia', 'sdsd@sdsd', '::1', '2015-04-22 05:38:52', 0, 0, '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hits`
--

CREATE TABLE IF NOT EXISTS `hits` (
  `home` varchar(50) NOT NULL,
  `other` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hits`
--

INSERT INTO `hits` (`home`, `other`) VALUES
('185', '627');

-- --------------------------------------------------------

--
-- Table structure for table `loggedin`
--

CREATE TABLE IF NOT EXISTS `loggedin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `times` int(11) NOT NULL,
  `last_ip` varchar(30) NOT NULL,
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `loggedin`
--

INSERT INTO `loggedin` (`id`, `user_id`, `times`, `last_ip`, `last_time`) VALUES
(1, 12, 0, '::1', '2014-07-06 23:32:59'),
(2, 12, 0, '::1', '2014-07-07 00:45:12'),
(3, 12, 0, '::1', '2014-07-08 01:30:35'),
(4, 12, 0, '::1', '2014-07-08 01:54:14'),
(5, 12, 0, '::1', '2014-07-08 02:26:27'),
(6, 12, 0, '::1', '2014-07-11 17:37:04'),
(7, 12, 0, '::1', '2014-07-12 12:51:37'),
(8, 12, 0, '::1', '2014-11-04 08:10:24'),
(9, 12, 0, '::1', '2014-11-04 09:03:43'),
(10, 12, 0, '::1', '2014-11-04 21:32:46'),
(11, 12, 0, '::1', '2014-11-29 12:48:43'),
(12, 12, 0, '::1', '2014-12-31 14:04:43'),
(13, 12, 0, '::1', '2015-01-12 22:00:26'),
(14, 12, 0, '::1', '2015-01-14 12:26:48'),
(15, 12, 0, '::1', '2015-01-15 21:36:01'),
(16, 12, 0, '::1', '2015-03-23 18:55:03'),
(17, 12, 0, '::1', '2015-03-23 18:57:26'),
(18, 12, 0, '::1', '2015-04-22 05:41:20'),
(19, 12, 0, '::1', '2015-04-22 06:50:06');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `headline` varchar(200) NOT NULL,
  `headline_id` varchar(400) NOT NULL,
  `body` varchar(5000) NOT NULL,
  `image` varchar(100) NOT NULL,
  `image2` varchar(100) NOT NULL,
  `top_news` int(11) NOT NULL,
  `more_news` int(11) NOT NULL,
  `posted_by` varchar(100) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hits` int(11) NOT NULL,
  `ip` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `cat_id`, `headline`, `headline_id`, `body`, `image`, `image2`, `top_news`, `more_news`, `posted_by`, `post_time`, `hits`, `ip`) VALUES
(2, 2, 'Emergency Meeting for easter 3', 'emergency_meeting_for_easter_3', '<p>Now in education, u wan try? Emergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easterEmergency Meeting for easter</p>', '2.jpg', '', 1, 1, '1', '2012-08-13 07:00:47', 38, '::1'),
(8, 1, 'another backend test', 'another_backend_test', '<p>hey hey her</p>\r\n<p>heheheheheh<br /></p>\r\n<h1><em><strong>hehehhekkkss</strong></em></h1>\r\n<p>&nbsp;</p>', '1another_backend_test.jpg', '', 0, 0, '', '2014-06-29 15:11:41', 5, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `pins`
--

CREATE TABLE IF NOT EXISTS `pins` (
  `pin` varchar(15) NOT NULL,
  `status` int(11) NOT NULL,
  `batch_no` varchar(40) NOT NULL,
  `serial_no` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`serial_no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `pins`
--

INSERT INTO `pins` (`pin`, `status`, `batch_no`, `serial_no`, `post_time`) VALUES
('1181084125', 1, '2014122003621', 00000000001, '2014-12-02 00:35:57'),
('1038891426', 1, '2015017013421', 00000000002, '2015-01-07 01:33:57'),
('3576391280', 1, '2015017013429', 00000000003, '2015-01-07 01:34:05'),
('1524891907', 1, '2015017013432', 00000000004, '2015-01-07 01:34:08'),
('1644741406', 1, '2015017013436', 00000000005, '2015-01-07 01:34:12'),
('1357915959', 1, '2015017013450', 00000000006, '2015-01-07 01:34:26'),
('1130608587', 1, '2015017013720', 00000000007, '2015-01-07 01:36:56'),
('1217732714', 0, '2015017013726', 00000000008, '2015-01-07 01:37:02'),
('7933076040', 0, '2015017013858', 00000000009, '2015-01-07 01:38:34'),
('3633325770', 0, '2015017013911', 00000000010, '2015-01-07 01:38:47'),
('1639909151', 0, '20150321144537', 00000000011, '2015-03-21 14:45:13');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` mediumtext NOT NULL,
  `cat` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `cat`, `round`) VALUES
(46, '__________ is the most valuable football club in the world', 6, 1),
(45, 'The First Champion of African cup of nation is ', 5, 1),
(44, 'What are the three official UEFA language', 7, 1),
(43, 'UEFA Champions league started in', 6, 1),
(42, 'NFF Stands for:', 4, 1),
(41, 'The first world cup was hosted by ', 1, 1),
(40, 'In what year was FIFA founded ', 4, 1),
(39, '__________ is the first Black African nation to qualify to the FIFA World cup ', 1, 1),
(38, 'Which is the most successfully team in the champions League', 3, 1),
(37, 'UEFA Stands for ', 1, 1),
(36, 'Thiery Henry Played for which of these clubs', 1, 1),
(35, 'The Arsenal  football club is nicknamed', 3, 1),
(34, 'Chelsea Football club is nicknamed ', 2, 1),
(33, 'The 1998 world cup was hosted by which of the following countries ', 1, 1),
(47, 'In what year was the African cup of nations founded', 4, 1),
(48, 'Which of the country has played  the most of the world cup matches', 5, 1),
(49, 'How many team contested the first world cup', 5, 1),
(50, 'According to FIFA the oldest football club', 4, 1),
(51, 'The first African cup of nations took place in ', 5, 1),
(52, 'Nigeria’s first involvement in the African cup of nations was in the year', 7, 1),
(53, 'The first CAF president is ', 5, 1),
(54, 'What led to the cancelation of 1946 World Cup', 4, 1),
(55, 'Who is the only African to be Russian Premier league top goal scorer', 9, 1),
(56, 'Who was the only goalkeeper to win the FIFA world cup golden ball (Best Player).', 8, 1),
(57, 'Which two men have won the FIFA world cup as both a player and a Manager', 10, 1),
(58, 'Which was the first Asian country to compete in the FIFA world cup finals', 8, 1),
(59, 'Name the only player in world cup history to have scored a hat-trick in two World cup competitions', 9, 1),
(60, 'Which country had the most number of players from its league at the 2014 FIFA world cup but failed to qualify itself for the world cup', 10, 1),
(61, 'Which Argentine player has scored the most FIFA world cup finals goals', 10, 1),
(62, 'Who is the only player to appear for two different nations at the FIFA world cup finals ', 8, 1),
(63, 'When was the first time a mascot was used for the FIFA world cup finals', 9, 1),
(64, 'Which team did Chelsea beat to win the 2013 Europa league final', 9, 1),
(65, 'Who was the first Barclays premier league manager to leave his Job September, 2013', 10, 1),
(66, 'The official match ball of the 2014 world cup is called', 8, 1),
(67, 'Which team finished bottom at the 2013 English premier League', 10, 1),
(68, 'tes question for round 2', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sms_settings`
--

CREATE TABLE IF NOT EXISTS `sms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_header` varchar(11) NOT NULL,
  `sms_username` varchar(100) NOT NULL,
  `sms_password` varchar(100) NOT NULL,
  `sms_email` varchar(200) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sms_settings`
--

INSERT INTO `sms_settings` (`id`, `sms_header`, `sms_username`, `sms_password`, `sms_email`, `last_updated`, `updated_by`) VALUES
(1, 'NFSC', 'ugdaniel', 'nigeria', 'ugdaniel@gmail.com', '2014-07-08 03:39:58', 12);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(200) NOT NULL,
  `code` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `code`) VALUES
(1, 'Abia', 'AB'),
(2, 'Adamawa', 'AD'),
(3, 'Akwa Ibom', 'AK'),
(4, 'Anambra', 'AN'),
(5, 'Bauchi', 'BA'),
(6, 'Bayelsa', 'BY'),
(7, 'Benue', 'BN'),
(8, 'Borno', 'BO'),
(9, 'Cross River', 'CR'),
(10, 'Delta', 'DT'),
(11, 'Ebonyi', 'EB'),
(12, 'Edo', 'ED'),
(13, 'Ekiti', 'EK'),
(14, 'Enugu', 'EN'),
(15, 'Federal Capital Territory', 'FC'),
(16, 'Gombe', 'GM'),
(17, 'Imo', 'IM'),
(18, 'Jigawa', 'JG'),
(19, 'Kaduna', 'KD'),
(20, 'Kano', 'KN'),
(21, 'Katsina', 'KT'),
(22, 'Kebbi', 'KB'),
(23, 'Kogi', 'KG'),
(24, 'Kwara', 'KW'),
(25, 'Lagos', 'LA'),
(26, 'Nasarawa', 'NS'),
(27, 'Niger', 'NI'),
(28, 'Ogun', 'OG'),
(29, 'Ondo', 'ON'),
(30, 'Osun', 'OS'),
(31, 'Oyo', 'OY'),
(32, 'Plateau', 'PL'),
(33, 'Rivers', 'RV'),
(34, 'Sokoto', 'SK'),
(35, 'Taraba', 'TB'),
(36, 'Yobe', 'YB'),
(37, 'Zamfara', 'ZM');

-- --------------------------------------------------------

--
-- Table structure for table `threats`
--

CREATE TABLE IF NOT EXISTS `threats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `area` varchar(50) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `threats`
--

INSERT INTO `threats` (`id`, `username`, `area`, `ip`, `post_time`) VALUES
(1, '', '3', '::1', '2015-01-15 21:35:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `suspend` int(11) NOT NULL,
  `role` varchar(10) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `phone`, `email`, `username`, `password`, `suspend`, `role`, `ip`, `status`, `post_time`, `image`) VALUES
(12, 'Developer', '', '', 'admin', 'nigeria', 0, '', '::1', 'Online', '2015-04-22 06:50:06', ''),
(13, 'ayo', '08097877441', 'nnejipatrickn@day.com', 'ayo', 'nigeria', 0, '', '::1', 'Offline', '2014-07-02 13:54:14', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
