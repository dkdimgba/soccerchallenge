<?php 
error_reporting(0);
include ('functions.php') ; ?>
    <!-- Favicons -->
    <link rel="shortcut icon" href="img/icons/favicon.ico">
    <link rel="apple-touch-icon" href="img/icons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-touch-icon-114x114.png">  

    <!-- Head Libs -->
    <script src="js/modernizr.js"></script>

    <!--[if lte IE 9]>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/r29/html5.js"></script>
        <script src="js/responsive/respond.js"></script>
    <![endif]-->

    <!-- styles for IE -->
    <!--[if lte IE 8]>
        <link rel="stylesheet" href="css/ie/ie.css" type="text/css" media="screen" />            
    <![endif]-->

    
    <!-- layout-->
    <div id="layout">

        <!-- Login Client -->
        <div class="jBar">
          <div class="container">            
              <div class="row">    

                  <div class="col-md-10">
                     <div class="row padding-top-mini">
                        <!-- Item service-->
                        <div class="col-md-4">
                            <div class="item-service border-right">
                                <div class="row head-service">
                                    <div class="col-md-2">
                                        <i class="fa fa-check-square"></i>                             
                                    </div>
                                    <div class="col-md-10">
                                        <h5>Register online</h5>
                                    </div>
                                </div>  
                                <p>Provide your name address and your favourite football club</p>
                            </div>
                        </div>      
                        <!-- End Item service-->

                        <!-- Item service-->
                        <div class="col-md-4">
                            <div class="item-service border-right">
                                <div class="row head-service">
                                    <div class="col-md-2">
                                        <i class="fa fa-star"></i>                             
                                    </div>
                                    <div class="col-md-10">
                                        <h5>Answer Questions online</h5>
                                    </div>
                                </div>  
                                <p>Partake in the online screening by answering 20 questions on the game of football.</p>
                            </div>
                        </div>      
                        <!-- End Item service-->

                        <!-- Item service-->
                        <div class="col-md-4">
                            <div class="item-service border-right">
                                <div class="row head-service">
                                    <div class="col-md-2">
                                        <i class="fa fa-desktop"></i>                             
                                    </div>
                                    <div class="col-md-10">
                                        <h5>Get on TV</h5>
                                    </div>
                                </div>  
                                <p>Get on the Live NSFC  TV Show and Prove your knowledge of the game. </p>
                            </div>
                        </div>      
                        <!-- End Item service-->
                     </div>
                  </div>

                  <div class="col-md-2">
                      <h5>Fan Login</h5>
<form action="play.php" method="post" name="form1" id="form1">
                        <input name="go" type="hidden" id="go" value="1" />  
                                                  <input type="name" placeholder="Email" name="email" required>
                           <input type="submit" class="btn btn-primary" value="Continue Gameshow">
                         <!-- <span>Or</span>                       
                          <input type="submit" class="btn btn-primary" value="Register">
                   -->   </form>
                  </div>

                            
                  <span class="jTrigger downarrow"><i class="fa fa-minus"></i></span>
              </div>
          </div>
      </div>
      <span class="jRibbon jTrigger up" title="Login">REGISTER <i class="fa fa-plus"></i></span>
      <div class="line"></div>
      <!-- End Login Client -->

        <!-- Info Head -->
        <section class="info-head">  
            <div class="container">
                <ul>  
                    <li><i class="fa fa-headphones"></i> 080-SOCCERFANS</li>
                    <li><i class="fa fa-comment"></i> <a href="chat.php/">Live chat</a></li>                    
                    <!--<li>
                        <ul>
                          <li class="dropdown">
                            <i class="fa fa-globe"></i> 
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Language<i class="fa fa-angle-down"></i>
                            </a>
                             <ul class="dropdown-menu">  
                                 <li><a href="#"><img src="img/language/spanish.png" alt="">Spanish</a></li>
                                 <li><a href="#"><img src="img/language/english.png" alt="">English</a></li>
                                 <li><a href="#"><img src="img/language/frances.png" alt="">Frances</a></li>
                                 <li><a href="#"><img src="img/language/portugues.png" alt="">Portuguese</a></li>
                            </ul>
                          </li>                      
                        </ul>
                    </li>-->
                </ul> 
            </div>            
        </section>
        <!-- Info Head -->

        <!-- Header-->
        <header class="animated fadeInDown delay1">           
            <div class="container">
                <div class="row">

                    <!-- Logo-->
                    <div class="col-md-3 logo">
                        <a href="index.php" title="Return Home">                            
                            <img src="css/skins/green/logo.png" alt="Logo" class="logo_img">
                        </a>
                    </div>
                    <!-- End Logo-->
                                                      
                    <!-- Nav-->
                    <nav class="col-md-9">
                        <!-- Menu-->
                        <ul id="menu" class="sf-menu">
                           
                            <li><a href="index.php">HOME</a></li>
                              <li><a href="about.php">GAME OVERVIEW</a>
                            <li><a href="play.php?mode=return">RETURNING FAN</a>
                                
                            <li>
                                <a href="register.php">REGISTER</a>
                                
                            </li>
                              <li><a href="invite-friend.php">INVITE A FRIEND</a></li>
                             
                                                                                                        
                             <li>
                                <a href="#">MEDIA <i class="fa fa-angle-down"></i></a>
                                <ul>                                  
                                    <li><a href="photo.php">PHOTOS</a></li>
                                    <li><a href="videos.php">VIDEOS</a></li>
                                    
                                </ul>
                            </li>
                       
                        </ul>
                        <!-- End Menu-->
                    </nav>
                    <!-- End Nav-->
                    
                </div><!-- End Row-->
            </div><!-- End Container-->
        </header>
        <!-- End Header-->