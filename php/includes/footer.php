

        <!-- Clients -->
        <section class="paddings clients">
            <div class="container">
               <div class="row">   

                    <!-- title-downloads -->             
                    <h1 class="title-downloads">
                        <span class="logo-clients">Over</span>  
                        <span class="responsive-numbers">
                            <span>2</span>
                            ,
                            <span>3</span>
                            <span>8</span>
                            <span>9</span>
                            ,
                            <span>5</span>
                            <span>1</span>
                            <span>8</span>
                        </span>
                         <span class="logo-clients">Nigerian Soccer Fans</span>
                        
                    </h1>  
                    <!-- End title-downloads -->     
                    
                    <!-- subtitle-downloads --> 
                    <div class="subtitle-downloads">
                        <div class="line"></div>
                        <h4>Official <i class="fa fa-star"></i> Sponsors</h4>
                    </div> 
                    <!-- End subtitle-downloads --> 

                    <!-- Image Clients Downloads --> 
                    <ul class="image-clients-downloads">
                        <li><img src="img/clients-downloads/10.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/1.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/2.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/3.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/4.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/5.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/6.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/7.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/8.jpg" alt=""></li>
                         <li><img src="img/clients-downloads/11.jpg" alt=""></li>
                    </ul>
                    <!-- End Image Clients Downloads --> 
               </div>                
            </div>
        </section>
        <!-- End Clients -->

<!-- Sponsors -->
        <section class="sponsors">
            <div class="overflow-sponsors">
                <div class="container paddings">

                   <h2>You are<span> 65 </span>questions away from becoming a millionaire</h2>
                                   
            </div>
                      
                             
                </div>
            </div>
        </section>
        <!-- End Sponsors -->
       
        

        <!-- footer bottom-->
        <footer class="footer-bottom">
            <div class="container">
               <div class="row">   
                                                                  
                    <!-- Nav-->
                    <div class="col-md-8">
                        <div class="logo-footer">
                            <h2><span>N</span>SFC<span>.</span></h2>
                        </div>
                        <!-- Menu-->
                        <ul class="menu-footer">
                            <li><a href="index.php">Home</a> </li>
                            <li><a href="about.php">About</a> </li>
                            <li><a href="winner.php">Winners</a></li>
                            <li><a href="faq.php">FAQ</a></li> 
                           
                            <li><a href="terms-conditions.php">Terms and Conditions</a></li>                                                     
                           
                           
                        </ul>
                        <!-- End Menu-->

                        <!-- coopring-->
                       <div class="row coopring">
                           <div class="col-md-8">
                               <p>&copy; 2015 NSFC . All Rights Reserved.</p>
                           </div>
                       </div>    
                       <!-- End coopring-->  

                    </div>
                    <!-- End Nav-->

                    <!-- Social-->
                    <div class="col-md-4">
                        <!-- Menu-->
                        <ul class="social">
                            <li data-toggle="tooltip" title data-original-title="Facebook">
                                <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Twitter">
                                <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Youtube">
                                <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>                     
                        </ul>
                        <!-- End Menu-->
                    </div>
                    <!-- End Social-->

               </div> 
                    
            </div>
        </footer>      
        <!-- End footer bottom-->

    </div>
    <!-- End layout-->

   