<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
     <title>Nigeria Soccer Fans Challenge | NSFC</title> 
      <meta name="keywords" content="HTML5 Design For NSFC" />
    <meta name="description" content="Nigeria Soccer Fans Challenge">
    <meta name="author" content="westwebtech.com">     

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link href="css/style.css" rel="stylesheet" media="screen">

    <!-- Skins Theme -->
    <link href="#" rel="stylesheet" media="screen" class="skin">

   <?php include 'php/includes/header.php';
?>
        <!-- End Header-->


        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="index.html">Home</a> / <a href="#">Photos </a>
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1>Photos
                            <span class="subtitle-section">
                                Photos
                                <span class="left"></span>
                                <span class="right"></span>
                            </span>
                            <span class="line-title"></span>
                        </h1>
                    </div>
                    <!-- End Title--> 

                    <!-- Search--> 
                    <div class="col-md-3">
                        <form class="search" action="#" method="Post">
                            <div class="input-group">
                                <input class="form-control" placeholder="Search..." name="email"  type="email" included="included">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" name="subscribe" >Go!</button>
                                </span>
                            </div>
                        </form>  
                    </div>
                    <!-- End Search--> 
                </div>
                <!-- End Title -Search --> 
              
            </div>
        </section>   
        <!-- End Title Section --> 


        <!-- Works -->
        <section class="paddings">
            <div class="container">

              

                <!-- Items Works filters-->
                <div class="works portfolioContainer">
                    
                    <!-- Item Work-->
                    <div class="col-md-4 desing">
                        <div class="item-work">
                            <div class="hover">
                                <img src="img/works/unilag.jpg" alt=""/>                               
                                 <a href="img/works/unilag.jpg" class="fancybox" title="Zoom Image"><div class="overlay"></div></a>
                            </div>                                   
                            <div class="info-work">
                                <h4>Winner</h4>
                                <p></p>
                              
                            </div>  
                        </div>
                    </div>
                    <!-- End Item Work-->
                 <!-- Item Work-->
                    <div class="col-md-4 desing">
                        <div class="item-work">
                            <div class="hover">
                                <img src="img/works/unilag-2.jpg" alt=""/>                               
                                 <a href="img/works/unilag-2.jpg" class="fancybox" title="Zoom Image"><div class="overlay"></div></a>
                            </div>                                   
                            <div class="info-work">
                                <h4>Winner</h4>
                                <p></p>
                              
                            </div>  
                        </div>
                    </div>
                    <!-- End Item Work-->
                     <!-- Item Work-->
                    <div class="col-md-4 desing">
                        <div class="item-work">
                            <div class="hover">
                                <img src="img/works/unilag-4.jpg" alt=""/>                               
                                 <a href="img/works/unilag-4.jpg" class="fancybox" title="Zoom Image"><div class="overlay"></div></a>
                            </div>                                   
                            <div class="info-work">
                                <h4>Winner</h4>
                                <p></p>
                              
                            </div>  
                        </div>
                    </div>
                    <!-- End Item Work-->
                     <!-- Item Work-->
                    <div class="col-md-4 desing">
                        <div class="item-work">
                            <div class="hover">
                                <img src="img/works/unilag-5.jpg" alt=""/>                               
                                 <a href="img/works/unilag-5.jpg" class="fancybox" title="Zoom Image"><div class="overlay"></div></a>
                            </div>                                   
                            <div class="info-work">
                                <h4>Winner</h4>
                                <p></p>
                              
                            </div>  
                        </div>
                    </div>
                    <!-- End Item Work-->
                     <!-- Item Work-->
                    <div class="col-md-4 desing">
                        <div class="item-work">
                            <div class="hover">
                                <img src="img/works/unliag3.jpg" alt=""/>                               
                                 <a href="img/works/unliag3.jpg" class="fancybox" title="Zoom Image"><div class="overlay"></div></a>
                            </div>                                   
                            <div class="info-work">
                                <h4>Winner</h4>
                                <p></p>
                              
                            </div>  
                        </div>
                    </div>
                    <!-- End Item Work-->
                     <!-- Item Work-->
                    <div class="col-md-4 desing">
                        <div class="item-work">
                            <div class="hover">
                                <img src="img/works/unilag-6.jpg" alt=""/>                               
                                 <a href="img/works/unilag-6.jpg" class="fancybox" title="Zoom Image"><div class="overlay"></div></a>
                            </div>                                   
                            <div class="info-work">
                                <h4>Winner</h4>
                                <p></p>
                              
                            </div>  
                        </div>
                    </div>
                    <!-- End Item Work-->
                    
                                
                </div>   
                <!-- End Items Works filters-->       
            </div>
            <!-- End Container-->
        </section>
        <!-- End Works-->
   

        <!-- footer top-->
              

        <!-- Clients -->
        <section class="paddings clients">
            <div class="container">
               <div class="row">   

                    <!-- title-downloads -->             
                    <h1 class="title-downloads">
                        <span class="logo-clients">Over</span>  
                        <span class="responsive-numbers">
                            <span>2</span>
                            ,
                            <span>3</span>
                            <span>8</span>
                            <span>9</span>
                            ,
                            <span>5</span>
                            <span>1</span>
                            <span>8</span>
                        </span>
                         <span class="logo-clients">Nigerian Soccer Fans</span>
                        
                    </h1>  
                    <!-- End title-downloads -->     
                    
                    <!-- subtitle-downloads --> 
                    <div class="subtitle-downloads">
                        <div class="line"></div>
                        <h4>Official <i class="fa fa-star"></i> Sponsors</h4>
                    </div> 
                    <!-- End subtitle-downloads --> 

                    <!-- Image Clients Downloads --> 
                    <ul class="image-clients-downloads">
                       <li><img src="img/clients-downloads/10.jpg" alt="Guaranty Trust Bank"></li>
                        <li><img src="img/clients-downloads/1.jpg" alt="Startimes"></li>
                        <li><img src="img/clients-downloads/2.jpg" alt="Africa Independence Television"></li>
                        <li><img src="img/clients-downloads/national.jpg" alt="National Lottery Regulatory Commision"></li>
                        <li><img src="img/clients-downloads/3.jpg" alt="Unitec Bank of Africa"></li>
                        <li><img src="img/clients-downloads/4.jpg" alt="Cool fm"></li>
                        <li><img src="img/clients-downloads/5.jpg" alt="Wazobia FM"></li>
                        <li><img src="img/clients-downloads/6.jpg" alt="Nigeria Info"></li>
                        <li><img src="img/clients-downloads/7.jpg" alt="Jumia"></li>
                        <li><img src="img/clients-downloads/8.jpg" alt="Global Lottery"></li>
                         <li><img src="img/clients-downloads/11.jpg" alt="Techformance Africa"></li>
                    </ul>
                    <!-- End Image Clients Downloads --> 
               </div>                
            </div>
        </section>
        <!-- End Clients -->

<!-- Sponsors -->
        <section class="sponsors">
            <div class="overflow-sponsors">
                <div class="container paddings">

                   <h2>You are<span> 65 </span>questions away from becoming a millionaire</h2>
                                   
            </div>
                      
                             
                </div>
            </div>
        </section>
        <!-- End Sponsors -->
       
        

        <!-- footer bottom-->
        <footer class="footer-bottom">
            <div class="container">
               <div class="row">   
                                                                  
                    <!-- Nav-->
                    <div class="col-md-8">
                        <div class="logo-footer">
                            <h2><span>N</span>SFC<span>.</span></h2>
                        </div>
                        <!-- Menu-->
                        <ul class="menu-footer">
                            <li><a href="index.php">Home</a> </li>
                            <li><a href="about.php">How It Works</a> </li>
                          <li><a href="photos.php">Photos</a></li>
                            <li><a href="tv-schedule.php">TV Schedule</a></li>
                            <li><a href="faq.php">FAQ</a></li> 
                           
                            <li><a href="terms-conditions.php">Terms and Conditions</a></li>                                                     
                           
                           
                        </ul>
                        <!-- End Menu-->

                        <!-- coopring-->
                       <div class="row coopring">
                           <div class="col-md-8">
                               <p>&copy; 2015 NSFC . All Rights Reserved.</p>
                           </div>
                       </div>    
                       <!-- End coopring-->  

                    </div>
                    <!-- End Nav-->

                    <!-- Social-->
                    <div class="col-md-4">
                        <!-- Menu-->
                        <ul class="social">
                            <li data-toggle="tooltip" title data-original-title="Facebook">
                                <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Twitter">
                                <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Youtube">
                                <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>                     
                        </ul>
                        <!-- End Menu-->
                    </div>
                    <!-- End Social-->

               </div> 
                    
            </div>
        </footer>      
        <!-- End footer bottom-->

    </div>
    <!-- End layout-->

   
    <!-- End layout-->

    <!-- ======================= JQuery libs =========================== -->
    <!-- Always latest version of jQuery-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <!-- jQuery local-->    
    <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>    
    <!--Nav-->
    <script type="text/javascript" src="js/nav/tinynav.js"></script> 
    <script type="text/javascript" src="js/nav/superfish.js"></script>  
    <script type="text/javascript" src="js/nav/hoverIntent.js"></script>  
    <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>                                          
    <!--Totop-->
    <script type="text/javascript" src="js/totop/jquery.ui.totop.js" ></script>  
    <!--Slide-->
    <script type="text/javascript" src="js/slide/camera.js" ></script>      
    <script type='text/javascript' src='js/slide/jquery.easing.1.3.min.js'></script>  
    <!--FlexSlider-->
    <script src="js/flexslider/jquery.flexslider.js"></script> 
    <!--Ligbox--> 
    <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script> 
    <!-- carousel.js-->
    <script src="js/carousel/carousel.js"></script>  
    <!-- Twitter Feed-->
    <script src="js/twitter/jquery.tweet.js"></script> 
    <!-- flickr Feed-->
    <script src="js/flickr/jflickrfeed.min.js"></script>  
    <!--Scroll-->   
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Nicescroll -->
    <script src="js/scrollbar/jquery.nicescroll.js"></script>
    <!-- Maps -->
    <script src="js/maps/gmap3.js"></script>
    <!-- Filter -->
    <script src="js/filters/jquery.isotope.js" type="text/javascript"></script>
    <!--Theme Options-->
    <script type="text/javascript" src="js/theme-options/theme-options.js"></script>
    <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>                               
    <!-- Bootstrap.js-->
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <!--MAIN FUNCTIONS-->
    <script type="text/javascript" src="js/main.js"></script>
    <!-- ======================= End JQuery libs =========================== -->
        
    </body>
</html>