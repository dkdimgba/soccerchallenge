<!DOCTYPE html>
<html lang="en">


  <head>
    <meta charset="utf-8">
    <title>Nigeria Soccer Fans Challenge | NSFC</title> 
    <meta name="keywords" content="Nigeria Soccer Fans Challenge -  NSFC" />
    <meta name="description" content="Nigeria Soccer Fans Challenge">
    <meta name="author" content="westwebtech.com">   

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link href="css/style.css" rel="stylesheet" media="screen">

    <!-- Skins Theme -->
    <link href="#" rel="stylesheet" media="screen" class="skin">

<?php include 'php/includes/header.php'; ?>
 <!-- Slide -->           
        <section class="camera_wrap camera_white_skin" id="slide">

             

             

            <!-- Item Slide style_one--> 
            <div  data-src="img/slide/slides/1.jpg">
                <div class="style_one">
                    <div class="container">
                        <div class="row">
                              
                          </div>
                    </div>                                                                                         
                </div>
            </div>
            <!-- End Item Slide style_one -->  
           

            <!-- Item Slide style_one--> 
            <div  data-src="img/slide/slides/3.jpg">
                <div class="style_one">
                    <div class="container">
                        <div class="row">
                              
                          </div>
                    </div>                                                                                         
                </div>
            </div>
            <!-- End Item Slide style_one -->  

           



        </section>   
        <!-- End Slide --> 

        


        <!-- box-action -->
        <section class="box-action">
            <div class="container">
                <div class="title">
                    <p class="lead">Dare to be crowned <span> THE MOST KNOWLEDGEABLE SOCCER FAN IN NIGERIA</span> </p>
                </div>
                <div>
                   

                    <a href="register.php">
  <img src="img/works/RegisterNow.png" alt="Register On Nigeria Soccer Fans Challenge" style="width:240px;height:74px;border:0">
</a>
                    <span class="arrow_box_action"></span>
                </div>
            </div>
        </section>
        <!-- End box-action-->


        <!-- Services -->
        <section class="paddings services position-relative">
            <div class="container">
                
                <i class="fa fa-money icon-section top right"></i>                

                <!-- Title Heading --> 
                <div class="titles-heading">
                    <div class="line"></div>
                    <p>The First and Biggest Soccer Fans <strong>Reality TV show</strong> where your knowledge of soccer can  win  you 7.5 Million Naira,  a trip to watch your favourite football team live or a brand new SUV</p>  
                     
                        <span>
                        <h3>  <i class="fa fa-star"></i>  
                    THE STEPS
                          <i class="fa fa-star"></i> </h3> 
                        </span>
                   
                </div> 
                <!-- End Title Heading --> 

              <!-- Row fuid-->
              <div class="row padding-top">
                 <!-- Item service-->
                  <div class="col-md-4">
                      <div class="item-service border-right">
                          <div class="row head-service">
                              <div class="col-md-2">
                                  <i class="fa fa-pencil"></i>                             
                              </div>
                              <div class="col-md-10">
                                  <h4>Register</h4>
                                  <h5>Provide your information</h5>
                              </div>
                          </div>                          
                          <p>It only takes few seconds, provide simple details such as name, state, email and football club<p>
                      </div>
                  </div>      
                  <!-- End Item service-->

                  <!-- Item service-->
                  <div class="col-md-4">
                      <div class="item-service border-right">
                          <div class="row head-service">
                              <div class="col-md-2">
                                 <i class="fa fa-fire"></i>                            
                              </div>
                              <div class="col-md-10">
                                  <h4>Answer Online Questions on Soccer</h4>
                                  <h5>earn points with every correct answer</h5>
                              </div>
                          </div>                          
                          <p> Each question will be displayed for 20 secs, Each Question carries 5 points, the higher your points the more your chances of winning.</p>
                      </div>
                  </div>      
                  <!-- End Item service-->

                  <!-- Item service-->
                  <div class="col-md-4">
                      <div class="item-service">
                          <div class="row head-service">
                              <div class="col-md-2">
                                  <i class="fa fa-mobile-phone (alias)"></i>                    
                              </div>
                              <div class="col-md-10">
                                  <h4>The Notification </h4>
                                  <h5>Successfully pass the three challenge stage</h5>
                              </div>
                          </div>                          
                          <p>Our agent will contact you for pre-auditioning, recieve a notification online and also get confirmation SMS</p>
                      </div>
                  </div>      
                  <!-- End Item service-->
                 
              </div>
              <!-- End Row fuid-->

             
                 
              </div>
              <!-- End Row fuid-->
               
            </div>
            <!-- End Container-->
        </section>
        <!-- End Services-->

<!-- Slides Team -->
        <section class="section-gray borders padding-top slide-team">
           
            <!-- Slide Team  -->
            <ul id="slide-team">
                    
                    <!-- Item Slide Team  -->
                    <li>
                        <div class="container">
                            <div class="row">

                                <!-- Image Team  -->
                                <div class="col-md-3">
                                   <a href="img/team-members/siasia.jpg" class="fancybox">
                                        <img src="img/team-members/siasia.jpg" alt="" title="View Image">
                                    </a>
                                </div>
                                <!-- End Image Team  -->
                                
                                <!-- Info  Team  -->
                                <div class="col-md-5 padding-top-mini">
                                  <h3 class="title-subtitle">
                                      Samson Siasia 
                                        <span>NSFC Game Ambassador</span>
                                   </h3>
                                   <p>Nigeria former striker and coach of the under 20,  under 23 and the super Eagle at different
period of His career. He has won various medals for the National team of such include
African Youth championship, Olympic medals Intercontinental cup just to mention a few
He's adjudged one of the most loved coach of the Nigeria National team.  
</p>


                                    <!-- Social-->
                                    <ul class="social">
                                        <li data-toggle="tooltip" title data-original-title="Facebook">
                                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </li> 
                                        <li data-toggle="tooltip" title data-original-title="Twitter">
                                            <a href="https://twitter.com/OfficialSiasia" target="_blank"><i class="fa fa-twitter"></i></a>
                                        </li> 
                                                           
                                    </ul>
                                    <!-- End Social-->
                                  
                               </div>
                               <!-- End Info  Team  -->
                                
                                

                           </div> 
                       </div> 
                    </li>
                    <!-- End Item Slide Team  -->

                     <!-- Item Slide Team  -->
                    <li>
                        <div class="container">
                            <div class="row">

                                <!-- Image Team  -->
                                <div class="col-md-3">
                                   <a href="img/team-members/emmanuel-babayaro.jpg" class="fancybox">
                                        <img src="img/team-members/babayaro.jpg" alt="" title="View Image">
                                    </a>
                                </div>
                                <!-- End Image Team  -->
                                
                                <!-- Info  Team  -->
                                <div class="col-md-5 padding-top-mini">
                                  <h3 class="title-subtitle">
                                        Emmanuel Babayaro(MON)
                                        <span>The Presenter and anchor of the Game show</span>
                                   </h3>
                                   <p>He has played in all the national teams from U17 to the Super Eagle. He's the President of
the Friends of Football Foundation, Executive Director Mothers of the Earth Foundation.
Goodwill Ambassador Friends Africa. He’s arguably Nigeria most sort after Football analyst.
</p>

                                   

                                    <!-- Social-->
                                    <ul class="social">
                                        <li data-toggle="tooltip" title data-original-title="Facebook">
                                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </li> 
                                        <li data-toggle="tooltip" title data-original-title="Twitter">
                                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                        </li> 
                                        <li data-toggle="tooltip" title data-original-title="Youtube">
                                            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                                        </li>                     
                                    </ul>
                                    <!-- End Social-->
                                  
                               </div>
                               <!-- End Info  Team  -->
                              

                           </div> 
                       </div> 
                    </li>
                    <!-- End Item Slide Team  -->

                    

                           </div> 
                       </div> 
                    </li>
                    <!-- End Item Slide Team  -->

                  
            </ul>  
            <!-- End Slide Team  -->  
        
        </section>
        

 <!-- Works Carousel -->
        <section class="paddings borders section-gray pattern-portfolios" id="work">
        
         <!-- Title Heading -->                
                <h1 class="text-center">NATIONWIDE TOUR</h1>               
                <!-- End Title Heading --> 
                 

                <!-- Items Works -->
                <ul id="works" class="works padding-top-mini">

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns14.jpg" alt="Image"/>                               
                             <a href="img/works/ns14.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                       <div class="info-work">
                            <h4> NSFC with Kanu</h4>
                            
                            
                        </div>  
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns22.jpg" alt="Image"/>                               
                             <a href="img/works/ns22.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                        
                        </div>  
                        <div class="info-work">
                            <h4> NSFC in Owerri</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns21.jpg" alt="Image"/>                               
                             <a href="img/works/ns21.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                      
                        </div>  
                        <div class="info-work">
                            <h4> NSFC in Kaduna</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns20.jpg" alt="Image"/>                               
                             <a href="img/works/ns20.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                        
                        </div>  
                        <div class="info-work">
                            <h4> NSFC in Lagos</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns19.jpg" alt="Image"/>                               
                             <a href="img/works/ns19.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                        
                        </div>  
                        <div class="info-work">
                            <h4> NSFC In Owerri</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns18.jpg" alt="Image"/>                               
                             <a href="img/works/ns18.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                       
                        </div>  
                        <div class="info-work">
                            <h4> NSFC in Port Harcourt</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns17.jpg" alt="Image"/>                               
                             <a href="img/works/ns17.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                       
                        </div> 
                        <div class="info-work">
                            <h4> NSFC in Port Harcourt</h4>
                            
                            
                        </div>  
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns16.jpg" alt="Image"/>                               
                             <a href="img/works/ns16.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                       
                        </div>  
                         <div class="info-work">
                            <h4> NSFC in Port Harcourt</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->



                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns15.jpg" alt="Image"/>                               
                             <a href="img/works/ns15.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                       
                        </div>  
                         <div class="info-work">
                            <h4> NSFC in Port Harcourt</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns13.jpg" alt="Image"/>                               
                             <a href="img/works/ns13.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                        
                        </div>  
                         <div class="info-work">
                            <h4> NSFC in Lagos</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns12.jpg" alt="Image"/>                               
                             <a href="img/works/ns12.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                      
                        </div>  
                         <div class="info-work">
                            <h4> NSFC in Owerri</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->


                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns11.jpg" alt="Image"/>                               
                             <a href="img/works/ns11.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                        
                        </div>  
                         <div class="info-work">
                            <h4> NSFC in Abuja</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->

                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="img/works/ns8.jpg" alt="Image"/>                               
                             <a href="img/works/ns8.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                       
                        </div>  
                         <div class="info-work">
                            <h4> NSFC in Kaduna</h4>
                            
                            
                        </div> 
                    </li>  
                    <!-- Item Work -->

                   
                   

                </ul>
                <!-- End Items Works -->
         
        </section>
        <!-- End Works Carousel-->

      

        <!-- Clients -->
        <section class="paddings clients">
            <div class="container">
               <div class="row">   

                    <!-- title-downloads -->             
                    <h1 class="title-downloads">
                        <span class="logo-clients">Over</span>  
                        <span class="responsive-numbers">
                            <span>2</span>
                            ,
                            <span>3</span>
                            <span>8</span>
                            <span>9</span>
                            ,
                            <span>5</span>
                            <span>1</span>
                            <span>8</span>
                        </span>
                         <span class="logo-clients">Nigerian Soccer Fans</span>
                        
                    </h1>  
                    <!-- End title-downloads -->     
                    
                    <!-- subtitle-downloads --> 
                    <div class="subtitle-downloads">
                        <div class="line"></div>
                        <h4>Official <i class="fa fa-star"></i> Partners</h4>
                    </div> 
                    <!-- End subtitle-downloads --> 

                    <!-- Image Clients Downloads --> 
                    <ul class="image-clients-downloads">
                        <li><img src="img/clients-downloads/10.jpg" alt="Guaranty Trust Bank"></li>
                        <li><img src="img/clients-downloads/1.jpg" alt="Startimes"></li>
                        <li><img src="img/clients-downloads/2.jpg" alt="Africa Independence Television"></li>
                        <li><img src="img/clients-downloads/national.jpg" alt="National Lottery Regulatory Commision"></li>
                        <li><img src="img/clients-downloads/3.jpg" alt="Unitec Bank of Africa"></li>
                        <li><img src="img/clients-downloads/4.jpg" alt="Cool fm"></li>
                        <li><img src="img/clients-downloads/5.jpg" alt="Wazobia FM"></li>
                        <li><img src="img/clients-downloads/6.jpg" alt="Nigeria Info"></li>
                        <li><img src="img/clients-downloads/7.jpg" alt="Jumia"></li>
                        <li><img src="img/clients-downloads/8.jpg" alt="Global Lottery"></li>
                         <li><img src="img/clients-downloads/11.jpg" alt="Techformance Africa"></li>
                    </ul>
                    <!-- End Image Clients Downloads --> 
               </div>                
            </div>
        </section>
        <!-- End Clients -->

<!-- Sponsors -->
        <section class="sponsors">
            <div class="overflow-sponsors">
                <div class="container paddings">
                   <h1 class="title-downloads">
                   
                         <span class="logo-clients">The Search for THE MOST KNOWLEDGEABLE SOCCER FAN IN NIGERIA has begun...</span>
                        
                    </h1>  

              
                                   
            </div>
                      
                             
                </div>
            </div>
        </section>
        <!-- End Sponsors -->
       
        

        <!-- footer bottom-->
        <footer class="footer-bottom">
            <div class="container">
               <div class="row">   
                                                                  
                    <!-- Nav-->
                    <div class="col-md-8">
                        <div class="logo-footer">
                            <h2><span>N</span>SFC<span>.</span></h2>
                        </div>
                        <!-- Menu-->
                        <ul class="menu-footer">
                            <li><a href="index.php">Home</a> </li>
                            <li><a href="about.php">How It Works</a> </li>
                           <li><a href="photos.php">Photos</a></li>
                            <li><a href="tv-schedule.php">TV Schedule</a></li>
                            <li><a href="faq.php">FAQ</a></li> 
                           
                            <li><a href="terms-conditions.php">Terms and Conditions</a></li>                                                     
                           
                           
                        </ul>
                        <!-- End Menu-->

                        <!-- coopring-->
                       <div class="row coopring">
                           <div class="col-md-8">
                               <p>&copy; 2015 NSFC . All Rights Reserved. NSFC  IS POWERED BY EL-RAE RESOURCE </p>
                           </div>
                       </div>    
                       <!-- End coopring-->  

                    </div>
                    <!-- End Nav-->

                    <!-- Social-->
                    <div class="col-md-4">
                        <!-- Menu-->
                        <ul class="social">
                            <li data-toggle="tooltip" title data-original-title="Facebook">
                                <a href="https://www.facebook.com/NGFansChallenge" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Twitter">
                                <a href="https://twitter.com/NGFansChallenge/" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Youtube">
                                <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>                     
                        </ul>
                        <!-- End Menu-->
                    </div>
                    <!-- End Social-->

               </div> 
                    
            </div>
        </footer>      
        <!-- End footer bottom-->

    </div>
    <!-- End layout-->

   

    <!-- ======================= JQuery libs =========================== -->
    <!-- Always latest version of jQuery-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <!-- jQuery local-->    
    <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>    
    <!--Nav-->
    <script type="text/javascript" src="js/nav/tinynav.js"></script> 
    <script type="text/javascript" src="js/nav/superfish.js"></script>  
    <script type="text/javascript" src="js/nav/hoverIntent.js"></script>  
    <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>                                          
    <!--Totop-->
    <script type="text/javascript" src="js/totop/jquery.ui.totop.js" ></script>  
    <!--Slide-->
    <script type="text/javascript" src="js/slide/camera.js" ></script>      
    <script type='text/javascript' src='js/slide/jquery.easing.1.3.min.js'></script>  
    <!--FlexSlider-->
    <script src="js/flexslider/jquery.flexslider.js"></script> 
    <!--Ligbox--> 
    <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script> 
    <!-- carousel.js-->
    <script src="js/carousel/carousel.js"></script>  
    <!-- Twitter Feed-->
    <script src="js/twitter/jquery.tweet.js"></script> 
    <!-- flickr Feed-->
    <script src="js/flickr/jflickrfeed.min.js"></script>  
    <!--Scroll-->   
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Nicescroll -->
    <script src="js/scrollbar/jquery.nicescroll.js"></script>
    <!-- Maps -->
    <script src="js/maps/gmap3.js"></script>
    <!-- Filter -->
    <script src="js/filters/jquery.isotope.js" type="text/javascript"></script>
    <!--Theme Options-->
    <script type="text/javascript" src="js/theme-options/theme-options.js"></script>
    <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>                               
    <!-- Bootstrap.js-->
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <!--MAIN FUNCTIONS-->
    <script type="text/javascript" src="js/main.js"></script>
    <!-- ======================= End JQuery libs =========================== -->
        
    </body>

</html>