 <?php require 'SquareLC/SquareLC.php';
?>

 <?php
  $settings = array
(
// This is the message on the top of the chat (set this to false or '' to remove the top bar, defaults to 'Live chat')
'title' => 'Chat room one',
// You can set these in either pixels or percents (default to 100%)
'width' => 650,
'height' => 400,
// Set this to true if you want the chat to be full screen, don't output any HTML on the page while using this (defaults to false)
'fullscreen' => false,
// Set this to false if you don't want to show the list of online users (defaults to true)
'view_online' => true,
// Set this to false if you want to remove the outer border of the chat (defaults to true)
'outer_border' => true,
// Set this to true if you want to show a send button next to the message input (defaults to false)
'view_send_btn' => true
);
 
SquareLC::chat($settings);
?>