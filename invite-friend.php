<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
       <meta name="keywords" content="HTML5 Design For NSFC" />
    <meta name="description" content="Nigeria Soccer Fans Challenge">
    <meta name="author" content="westwebtech.com">     

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link href="css/style.css" rel="stylesheet" media="screen">

    <!-- Skins Theme -->
    <link href="#" rel="stylesheet" media="screen" class="skin">
     <link rel="stylesheet" href="css/elegant.css">

   <?php include 'php/includes/header.php';
?>


        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="index.html">Home</a> / <a href="#">IInvite a friend</a>
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1>Invite a friend

                           
                        </h1>
                    </div>
                    <!-- End Title--> 

                    <!-- Search--> 
                    <div class="col-md-3">
                        <form class="search" action="#" method="Post">
                            <div class="input-group">
                                <input class="form-control" placeholder="Search..." name="email"  type="email" included="included">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" name="subscribe" >Go!</button>
                                </span>
                            </div>
                        </form>  
                    </div>
                    <!-- End Search--> 
                </div>
                <!-- End Title -Search --> 
              
            </div>
        </section>   
        <!-- End Title Section --> 


        

<!-- Sponsors -->
        <section class="register">
            <div class="overflow-register">
                <div class="container paddings">


<!-- Works -->
        <section class="paddings">
            <div class="container">
                <div class="row">   


                    <div class="col-md-8">

     <form action="post" class="elegant-aero">
         <h1>Invite a friend</h1>
         <span> Please fill the form below to invite a friend to the challenge </span>
         <br></br>
    <fieldset>
        <div >
            <label for="name">Full Name</label>
            <input id="name" type="text" placeholder="Name">
        </div>

            <div class="pure-control-group">
            <label for="email">Email Address</label>
            <input id="email" type="email" placeholder="Email Address">
        </div>

        <div>
            <label for="password">Phone Number</label>
            <input id="number" type="number" placeholder="Phone">
        </div>

    

        

        <div class="pure-controls">
            <label for="cb" class="pure-checkbox">
                <input id="cb" type="checkbox"> I have read the <a href="terms-conditions.php">Terms and Conditions</a>
            </label>

            <button type="submit" class="pure-button pure-button-primary">Submit</button>
        </div>
    </fieldset>
</form>
                   
                    </div>               
                   
                    <!-- Sidebars -->
                    <?php include 'php/includes/sidebars.php';
?>
                    <!-- End Sidebars -->


                </div>
            </div>
            <!-- End Container-->
        </section>
        <!-- End Works-->
   

      <!-- Clients -->
        <section class="paddings clients">
            <div class="container">
               <div class="row">   
    
                    
                    <!-- subtitle-downloads --> 
                    <div class="subtitle-downloads">
                        <div class="line"></div>
                        <h4>Official <i class="fa fa-star"></i> Partners</h4>
                    </div> 
                    <!-- End subtitle-downloads --> 

                    <!-- Image Clients Downloads --> 
                    <ul class="image-clients-downloads">
                     <li><img src="img/clients-downloads/10.jpg" alt="Guaranty Trust Bank"></li>
                        <li><img src="img/clients-downloads/1.jpg" alt="Startimes"></li>
                        <li><img src="img/clients-downloads/2.jpg" alt="Africa Independence Television"></li>
                        <li><img src="img/clients-downloads/national.jpg" alt="National Lottery Regulatory Commision"></li>
                        <li><img src="img/clients-downloads/3.jpg" alt="Unitec Bank of Africa"></li>
                        <li><img src="img/clients-downloads/4.jpg" alt="Cool fm"></li>
                        <li><img src="img/clients-downloads/5.jpg" alt="Wazobia FM"></li>
                        <li><img src="img/clients-downloads/6.jpg" alt="Nigeria Info"></li>
                        <li><img src="img/clients-downloads/7.jpg" alt="Jumia"></li>
                        <li><img src="img/clients-downloads/8.jpg" alt="Global Lottery"></li>
                         <li><img src="img/clients-downloads/11.jpg" alt="Techformance Africa"></li>
                    </ul>
                    <!-- End Image Clients Downloads --> 
               </div>                
            </div>
        </section>
        <!-- End Clients -->
                  
                                   
            </div>
                      
                             
                </div>
            </div>
        </section>
        <!-- End Sponsors -->
       
        

        <!-- footer bottom-->
        <footer class="footer-bottom">
            <div class="container">
               <div class="row">   
                                                                  
                    <!-- Nav-->
                    <div class="col-md-8">
                        <div class="logo-footer">
                            <h2><span>N</span>SFC<span>.</span></h2>
                        </div>
                        <!-- Menu-->
                        <ul class="menu-footer">
                            <li><a href="index.php">Home</a> </li>
                            <li><a href="about.php">How It Works</a> </li>
                             <li><a href="winner.php">Awards</a></li>
                            <li><a href="tv-schedule.php">TV Schedule</a></li>
                            <li><a href="faq.php">FAQ</a></li> 
                           
                            <li><a href="terms-conditions.php">Terms and Conditions</a></li>                                                     
                           
                           
                        </ul>
                        <!-- End Menu-->

                        <!-- coopring-->
                       <div class="row coopring">
                           <div class="col-md-8">
                               <p>&copy; 2015 NSFC . All Rights Reserved.</p>
                           </div>
                       </div>    
                       <!-- End coopring-->  

                    </div>
                    <!-- End Nav-->

                    <!-- Social-->
                    <div class="col-md-4">
                        <!-- Menu-->
                        <ul class="social">
                            <li data-toggle="tooltip" title data-original-title="Facebook">
                                <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Twitter">
                                <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Youtube">
                                <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>                     
                        </ul>
                        <!-- End Menu-->
                    </div>
                    <!-- End Social-->

               </div> 
                    
            </div>
        </footer>      
        <!-- End footer bottom-->

    </div>
    <!-- End layout-->

   
    <!-- End layout-->

    <!-- ======================= JQuery libs =========================== -->
    <!-- Always latest version of jQuery-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <!-- jQuery local-->    
    <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>    
    <!--Nav-->
    <script type="text/javascript" src="js/nav/tinynav.js"></script> 
    <script type="text/javascript" src="js/nav/superfish.js"></script>  
    <script type="text/javascript" src="js/nav/hoverIntent.js"></script> 
    <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>                                           
    <!--Totop-->
    <script type="text/javascript" src="js/totop/jquery.ui.totop.js" ></script>  
    <!--Slide-->
    <script type="text/javascript" src="js/slide/camera.js" ></script>      
    <script type='text/javascript' src='js/slide/jquery.easing.1.3.min.js'></script>  
    <!--FlexSlider-->
    <script src="js/flexslider/jquery.flexslider.js"></script> 
    <!--Ligbox--> 
    <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script> 
    <!-- carousel.js-->
    <script src="js/carousel/carousel.js"></script>  
    <!-- Twitter Feed-->
    <script src="js/twitter/jquery.tweet.js"></script> 
    <!-- flickr Feed-->
    <script src="js/flickr/jflickrfeed.min.js"></script>  
    <!--Scroll-->   
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Nicescroll -->
    <script src="js/scrollbar/jquery.nicescroll.js"></script>
    <!-- Maps -->
    <script src="js/maps/gmap3.js"></script>
    <!-- Filter -->
    <script src="js/filters/jquery.isotope.js" type="text/javascript"></script>
    <!--Theme Options-->
    <script type="text/javascript" src="js/theme-options/theme-options.js"></script>
    <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>                                
    <!-- Bootstrap.js-->
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <!--MAIN FUNCTIONS-->
    <script type="text/javascript" src="js/main.js"></script>
    <!-- ======================= End JQuery libs =========================== -->
        
    </body>
</html>