<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Nigeria Soccer Fans Challenge | NSFC</title> 
       <meta name="keywords" content="HTML5 Design For NSFC" />
    <meta name="description" content="Nigeria Soccer Fans Challenge">
    <meta name="author" content="westwebtech.com">     

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link href="css/style.css" rel="stylesheet" media="screen">

    <!-- Skins Theme -->
    <link href="#" rel="stylesheet" media="screen" class="skin">

   <?php include 'php/includes/header.php';
?>
        
        <!-- End Header-->
        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="index.html">Home</a>  / <a href="tv-schedule.html">TV Schedule</a>
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1>TV Schedule
                            <span class="subtitle-section">
                               Stations
                                <span class="left"></span>
                                <span class="right"></span>
                            </span>
                            <span class="line-title"></span>
                        </h1>
                    </div>
                    <!-- End Title--> 

                    <!-- Search--> 
                    <div class="col-md-3">
                        <form class="search" action="#" method="Post">
                            <div class="input-group">
                                <input class="form-control" placeholder="Search..." name="email"  type="email" included>
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" name="subscribe" >Go!</button>
                                </span>
                            </div>
                        </form>  
                    </div>
                    <!-- End Search--> 
                </div>
                <!-- End Title -Search --> 
              
            </div>
        </section>   
        <!-- End Title Section --> 


        <!-- Box Services--> 
        <section class="padding-bottom">
            <div class="container">
               <div class="row">  
                    <!-- More services --> 
                    <div class="col-md-12">
                        
                        

                         <!-- Box -->
                        <ul class="box">

                            <!-- Item More-service --> 
                            <li class="row">
                                <!-- Image Services --> 
                                <div class="col-md-3">
                                    <div class="image-more-service">
                                    <img src="img/tv/nta_1.jpg" width="132" height="99"> </div>
                                </div>
                                <!-- End Image Services --> 
                                <div class="col-md-7">
                                    <div class="info">
                                        <h4>NTA</h4>
                                        <p>Saturday 3:30pm - 4.00pm </p>
                                    </div>
                                </div>
                               
                            </li>
                            <!-- End Item More-service --> 

                           
                            <!-- Item More-service --> 
                            <li class="row">
                                <!-- Image Services --> 
                                <div class="col-md-3">
                                    <div class="image-more-service">
                                    <img src="img/tv/s_tv_1.jpg" width="132" height="99"> </div>
                                </div>
                                <!-- End Image Services --> 
                                <div class="col-md-7">
                                    <div class="info">
                                        <h4>STV</h4>
                                        <p>Saturday 3:30pm - 4.00pm </p>
                                    </div>
                                </div>
                               
                            </li>
                            <!-- End Item More-service -->
                            
                           
                            <!-- Item More-service --> 
                            <li class="row">
                                <!-- Image Services --> 
                                <div class="col-md-3">
                                    <div class="image-more-service">
                                    <img src="img/tv/cool_1.jpg" width="132" height="99"> </div>
                                </div>
                                <!-- End Image Services --> 
                                <div class="col-md-7">
                                    <div class="info">
                                        <h4>COOL TV</h4>
                                        <p>Saturday 3:30pm - 4.00pm </p>
                                    </div>
                                </div>
                               
                            </li>
                            <!-- End Item More-service -->
                            
                            <!-- Item More-service --> 
                            <li class="row">
                                <!-- Image Services --> 
                                <div class="col-md-3">
                                    <div class="image-more-service">
                                    <img src="img/tv/superscreen_1.jpg" width="132" height="99"> </div>
                                </div>
                                <!-- End Image Services --> 
                                <div class="col-md-7">
                                    <div class="info">
                                        <h4>Super Screen</h4>
                                        <p>Saturday 3:30pm - 4.00pm </p>
                                    </div>
                                </div>
                               
                            </li>
                            <!-- End Item More-service -->
                            
                            <!-- Item More-service --> 
                            <li class="row">
                                <!-- Image Services --> 
                                <div class="col-md-3">
                                    <div class="image-more-service">
                                    <img src="img/tv/wazobie_tv_1.jpg" width="132" height="99"> </div>
                                </div>
                                <!-- End Image Services --> 
                                <div class="col-md-7">
                                    <div class="info">
                                        <h4>WAZOBIA</h4>
                                        <p>Saturday 3:30pm - 4.00pm </p>
                                    </div>
                                </div>
                               
                            </li>
                            <!-- End Item More-service --> 
                        </ul>
                        <!-- End Box -->
                    </div>
                    <!-- End More services --> 
</div>
        </section>
        <!-- footer top-->
              <!-- footer top-->
              

        <!-- Clients -->
        <section class="paddings clients">
            <div class="container">
               <div class="row">   

                    <!-- title-downloads -->             
                    <h1 class="title-downloads">
                        <span class="logo-clients">Over</span>  
                        <span class="responsive-numbers">
                            <span>2</span>
                            ,
                            <span>3</span>
                            <span>8</span>
                            <span>9</span>
                            ,
                            <span>5</span>
                            <span>1</span>
                            <span>8</span>
                        </span>
                         <span class="logo-clients">Nigerian Soccer Fans</span>
                        
                    </h1>  
                    <!-- End title-downloads -->     
                    
                    <!-- subtitle-downloads --> 
                    <div class="subtitle-downloads">
                        <div class="line"></div>
                        <h4>Official <i class="fa fa-star"></i> Sponsors</h4>
                    </div> 
                    <!-- End subtitle-downloads --> 

                    <!-- Image Clients Downloads --> 
                    <ul class="image-clients-downloads">
                        <li><img src="img/clients-downloads/10.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/1.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/2.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/3.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/4.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/5.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/6.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/7.jpg" alt=""></li>
                        <li><img src="img/clients-downloads/8.jpg" alt=""></li>
                         <li><img src="img/clients-downloads/11.jpg" alt=""></li>
                    </ul>
                    <!-- End Image Clients Downloads --> 
               </div>                
            </div>
        </section>
        <!-- End Clients -->

<!-- Sponsors -->
        <section class="sponsors">
            <div class="overflow-sponsors">
                <div class="container paddings">

                   <h2>You are<span> 65 </span>questions away from becoming a millionaire</h2>
                                   
            </div>
                      
                             
                </div>
            </div>
        </section>
        <!-- End Sponsors -->
       
        

        <!-- footer bottom-->
        <footer class="footer-bottom">
            <div class="container">
               <div class="row">   
                                                                  
                    <!-- Nav-->
                    <div class="col-md-8">
                        <div class="logo-footer">
                            <h2><span>N</span>SFC<span>.</span></h2>
                        </div>
                        <!-- Menu-->
                        <ul class="menu-footer">
                            <li><a href="index.php">Home</a> </li>
                            <li><a href="about.php">How It Works</a> </li>
                            <li><a href="photos.php">Photos</a></li>
                            <li><a href="tv-schedule.php">TV Schedule</a></li>
                            <li><a href="faq.php">FAQ</a></li> 
                           
                            <li><a href="terms-conditions.php">Terms and Conditions</a></li>                                                     
                           
                           
                        </ul>
                        <!-- End Menu-->

                        <!-- coopring-->
                       <div class="row coopring">
                           <div class="col-md-8">
                               <p>&copy; 2015 NSFC . All Rights Reserved.</p>
                           </div>
                       </div>    
                       <!-- End coopring-->  

                    </div>
                    <!-- End Nav-->

                    <!-- Social-->
                    <div class="col-md-4">
                        <!-- Menu-->
                        <ul class="social">
                            <li data-toggle="tooltip" title data-original-title="Facebook">
                                <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Twitter">
                                <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Youtube">
                                <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>                     
                        </ul>
                        <!-- End Menu-->
                    </div>
                    <!-- End Social-->

               </div> 
                    
            </div>
        </footer>      
        <!-- End footer bottom-->

    </div>
    <!-- End layout-->

   
    <!-- End layout-->

    <!-- ======================= JQuery libs =========================== -->
    <!-- Always latest version of jQuery-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <!-- jQuery local-->    
    <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>    
    <!--Nav-->
    <script type="text/javascript" src="js/nav/tinynav.js"></script> 
    <script type="text/javascript" src="js/nav/superfish.js"></script>  
    <script type="text/javascript" src="js/nav/hoverIntent.js"></script> 
    <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>                                           
    <!--Totop-->
    <script type="text/javascript" src="js/totop/jquery.ui.totop.js" ></script>  
    <!--Slide-->
    <script type="text/javascript" src="js/slide/camera.js" ></script>      
    <script type='text/javascript' src='js/slide/jquery.easing.1.3.min.js'></script>  
    <!--FlexSlider-->
    <script src="js/flexslider/jquery.flexslider.js"></script> 
    <!--Ligbox--> 
    <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script> 
    <!-- carousel.js-->
    <script src="js/carousel/carousel.js"></script>  
    <!-- Twitter Feed-->
    <script src="js/twitter/jquery.tweet.js"></script> 
    <!-- flickr Feed-->
    <script src="js/flickr/jflickrfeed.min.js"></script>  
    <!--Scroll-->   
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Nicescroll -->
    <script src="js/scrollbar/jquery.nicescroll.js"></script>
    <!-- Maps -->
    <script src="js/maps/gmap3.js"></script>
    <!-- Filter -->
    <script src="js/filters/jquery.isotope.js" type="text/javascript"></script>
    <!--Theme Options-->
    <script type="text/javascript" src="js/theme-options/theme-options.js"></script>
    <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>                                
    <!-- Bootstrap.js-->
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <!--MAIN FUNCTIONS-->
    <script type="text/javascript" src="js/main.js"></script>
    <!-- ======================= End JQuery libs =========================== -->
        
    </body>
</html>