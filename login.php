<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

 <head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="Nigeria Soccer Fans Challenge" />
  <meta name="title" content="Over N20,000,000 (twenty million) Naira to be won in the 'Most Knowledgeable Soccer Fan Challenge'" />
  <meta name="description" content="" />
  <title>Nigeria Soccer Fans Challenge - Login</title>

<!-- ////////////////////////////////// -->
<!-- //      Start Stylesheets       // -->
<!-- ////////////////////////////////// -->

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/style2.css" rel="stylesheet" type="text/css" />
<!-- ////////////////////////////////// -->
<!-- //      Javascript Files        // -->
<!-- ////////////////////////////////// -->
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script src="js/jquery.cycle.all.js" type="text/javascript"></script>
<script type="text/javascript">
   $(function(){ 
		 $('.bottomtext').cycle({
            timeout: 5000,  // milliseconds between slide transitions (0 to disable auto advance)
            fx:      'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...            
            pause:   0,	  // true to enable "pause on hover"
			cleartypeNoBg: true, // set to true to disable extra cleartype fixing (leave false to force background color setting on slides)
            pauseOnPagerHover: 0 // true to pause when hovering over pager link
        });
     });
</script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/Hattori_Hanzo_300-Hattori_Hanzo_italic_300.font.js"></script>
<script type="text/javascript">
            Cufon.replace('h1') ('h2') ('h3') ('h4') ('h5') ('h6') ('#main-nav li a', { 
				hover: true,
				textShadow: '0px 1px 0px #9db65c'
			 }) ('.searchtext');
</script>
<!--[if IE 6]>    
    <script type="text/javascript" src="js/DD_belatedPNG.js"></script>
	<script type="text/javascript"> 
	   DD_belatedPNG.fix('img'); 
	</script>    
<![endif]-->
<!--[if IE 7]>    
    <style type="text/css">
       .bottomtext li{margin:10px 0px 0px 0px;}
	</style>    
<![endif]-->
 
</head>
<body>
<div id="topcontainer-inner">
    
    	<!-- BEGIN OF CENTER COLUMN -->
    	<div class="centercolumn">
      	  <div id="topbg2">
          
        	<!-- BEGIN OF TOP -->
        	<div id="top">
            	<div id="logo"><a href="#"><img src="images/logo.png" alt="" /></a></div>
                <!--<div id="searchtop">
                	<form method="post" action="#">
                    <div class="searchtext">search something?</div>
                    <div class="bgsearch"><input type="text" class="inputsearch" /></div>
                    </form>
                </div>-->
            </div>
            <!-- END OF TOP -->
            
        	<!-- BEGIN OF HEADER -->
        	<div id="header-inner">
            	<h1 class="title" style="color:#FFF">Login</h1>
            </div>
            <!-- END OF HEADER -->
            
            </div>
            
        	<!-- BEGIN OF CONTENT BOX -->
        	<div id="contentbox">
                                 <?php //include ('nav.php') ; ?>

            </div>
            <!-- END OF CONTENT BOX -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content">
                    <div class="contact-area">                             
                                <div id="contactFormArea">
                                <?php 	
								
								$mode = $_GET['mode'];
								
								if($mode=="no_login"){
								echo "<div class=\"error\"> You Must Be Logged in to Proceed </div><br />" ;
								}
								else if($mode=="loggedout")
								{
								echo "<div class=\"error\"> You Are Logged Out Successfully </div><br />" ;
								}
								
 
?>
                                      <!-- Contact Form Start //-->
                                      <form action="processlogin.php" id="contactform" method="post"> 
                                      <fieldset>
                                      <label>Username</label>
                                      <input type="text" name="username" class="textfield" id="username" value="" />
                                      <div class="clear"></div>
                                      <label>Password</label>
                                      <input type="password" name="password" class="textfield" id="password" value="" />
                                      <div class="clear"></div>
                                      
                                       <label>&nbsp;</label>
                                      <input type="submit" name="submit" class="buttoncontact" id="buttonsend" value="Login" />
                                      <span class="loading" style="display: none;">Please wait..</span>
                                      <div class="clear"></div>
                                      </fieldset> 
                                      
                                       </form>
                                      <!-- Contact Form End //-->  <br />

                                         <br />


                            	</div><br />
<br />
<br /><br />
<br />

<br />
<br />
<br />
<br />
                           </div>

            </div>
            <!-- END OF CONTENT -->
            
        </div>
        <!-- END OF CENTER COLUMN -->
        
    </div>
    <!-- END OF TOP CONTAINER -->
    
	<!-- BEGIN OF BOTTOM CONTAINER -->
	<div id="bottomcontainer">
    
    	<!-- BEGIN OF CENTER COLUMN -->
    	<?php // include ('footer_slide.php') ; ?>
        <!-- END OF CENTER COLUMN -->
        
    </div>
    <!-- END OF BOTTOM CONTAINER -->
    
	<!-- BEGIN OF FOOTER CONTAINER -->
 <?php include ('footer.php') ; ?>

    <!-- END OF FOOTER CONTAINER -->
    
</body>

 </html>