<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
  <title>Nigeria Soccer Fans Challenge | NSFC</title> 
       <meta name="keywords" content="HTML5 Design For NSFC" />
    <meta name="description" content="Nigeria Soccer Fans Challenge">
    <meta name="author" content="westwebtech.com">     

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link href="css/style.css" rel="stylesheet" media="screen">

    <!-- Skins Theme -->
    <link href="#" rel="stylesheet" media="screen" class="skin">

   <?php include 'php/includes/header.php';
?>


        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="index.php">Home</a> / <a href="#">Winners</a> 
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1>Winners
                            <span class="subtitle-section">
                                Winners' Profile
                                <span class="left"></span>
                                <span class="right"></span>
                            </span>
                            <span class="line-title"></span>
                        </h1>
                    </div>
                    <!-- End Title--> 

                    <!-- Search--> 
                    <div class="col-md-3">
                        <form class="search" action="#" method="Post">
                            <div class="input-group">
                                <input class="form-control" placeholder="Search..." name="email"  type="email" included="included">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" name="subscribe" >Go!</button>
                                </span>
                            </div>
                        </form>  
                    </div>
                    <!-- End Search--> 
                </div>
                <!-- End Title -Search --> 
              
            </div>
        </section>   
        <!-- End Title Section --> 


        <!-- Works -->
        <section class="paddings">
            <div class="container">
                <div class="row">   


                    <div class="col-md-8">
                        <h2>Winners' Profile</h2>
                        <p class="lead">
                             Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh metus.
                        </p>

                        <hr class="tall">

                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>

                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>

                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>

                        <p>Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Vestibulum ante ipsum primis in faucibus orci.</p>

                    </div>               
                   
                    <!-- Sidebars -->
                    <div class="col-md-4 sidebars">

                        <a href="about.html"><img src="img/works/hyundai.jpg" alt="Hyundai Santa Fe"></a>
                       
                        <aside>
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                     <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Popular</a></li>
                                    <li class=""><a href="#recentPosts" data-toggle="tab">Recent</a></li>
                                 </ul>
                                <div class="tab-content">

                                    <div class="tab-pane active" id="popularPosts">
                                        <ul class="simple-post-list">
                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="img/clients-downloads/1.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>

                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="img/clients-downloads/2.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>

                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="img/clients-downloads/3.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>
                                              
                                        </ul>
                                    </div>

                                    <div class="tab-pane" id="recentPosts">
                                        <ul class="simple-post-list">
                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                             <img src="img/clients-downloads/3.jpg" alt="">
                                                        </a>
                                                    </div>
                                                 </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                         Nov 25 / 7 / 2013
                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="img/clients-downloads/2.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>

                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="img/clients-downloads/1.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>
                                              
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </aside>

                        <aside>
                            <h4>Wiget Text</h4>
                            <p>Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Nulla nunc dui, tristique in semper vel. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero.</p>
                        </aside>

                    </div>
                    <!-- End Sidebars -->


                </div>
            </div>
            <!-- End Container-->
        </section>
        <!-- End Works-->
   

        <!-- footer top-->
               <!-- footer top-->
              

        <!-- Clients -->
        <section class="paddings clients">
            <div class="container">
               <div class="row">   

                    <!-- title-downloads -->             
                    <h1 class="title-downloads">
                        <span class="logo-clients">Over</span>  
                        <span class="responsive-numbers">
                            <span>2</span>
                            ,
                            <span>3</span>
                            <span>8</span>
                            <span>9</span>
                            ,
                            <span>5</span>
                            <span>1</span>
                            <span>8</span>
                        </span>
                         <span class="logo-clients">Nigerian Soccer Fans</span>
                        
                    </h1>  
                    <!-- End title-downloads -->     
                    
                    <!-- subtitle-downloads --> 
                    <div class="subtitle-downloads">
                        <div class="line"></div>
                        <h4>Official <i class="fa fa-star"></i> Sponsors</h4>
                    </div> 
                    <!-- End subtitle-downloads --> 

                    <!-- Image Clients Downloads --> 
                    <ul class="image-clients-downloads">
                                <li><img src="img/clients-downloads/10.jpg" alt="Guaranty Trust Bank"></li>
                        <li><img src="img/clients-downloads/1.jpg" alt="Startimes"></li>
                        <li><img src="img/clients-downloads/2.jpg" alt="Africa Independence Television"></li>
                        <li><img src="img/clients-downloads/national.jpg" alt="National Lottery Regulatory Commision"></li>
                        <li><img src="img/clients-downloads/3.jpg" alt="Unitec Bank of Africa"></li>
                        <li><img src="img/clients-downloads/4.jpg" alt="Cool fm"></li>
                        <li><img src="img/clients-downloads/5.jpg" alt="Wazobia FM"></li>
                        <li><img src="img/clients-downloads/6.jpg" alt="Nigeria Info"></li>
                        <li><img src="img/clients-downloads/7.jpg" alt="Jumia"></li>
                        <li><img src="img/clients-downloads/8.jpg" alt="Global Lottery"></li>
                         <li><img src="img/clients-downloads/11.jpg" alt="Techformance Africa"></li>
                    </ul>
                    <!-- End Image Clients Downloads --> 
               </div>                
            </div>
        </section>
        <!-- End Clients -->

<!-- Sponsors -->
        <section class="sponsors">
            <div class="overflow-sponsors">
                <div class="container paddings">

                   <h2>You are<span> 65 </span>questions away from becoming a millionaire</h2>
                                   
            </div>
                      
                             
                </div>
            </div>
        </section>
        <!-- End Sponsors -->
       
        

        <!-- footer bottom-->
        <footer class="footer-bottom">
            <div class="container">
               <div class="row">   
                                                                  
                    <!-- Nav-->
                    <div class="col-md-8">
                        <div class="logo-footer">
                            <h2><span>N</span>SFC<span>.</span></h2>
                        </div>
                        <!-- Menu-->
                        <ul class="menu-footer">
                            <li><a href="index.php">Home</a> </li>
                            <li><a href="about.php">How It Works</a> </li>
                            <li><a href="photos.php">Photos</a></li>
                            <li><a href="tv-schedule.php">TV Schedule</a></li>
                            <li><a href="faq.php">FAQ</a></li> 
                           
                            <li><a href="terms-conditions.php">Terms and Conditions</a></li>                                                     
                           
                           
                        </ul>
                        <!-- End Menu-->

                        <!-- coopring-->
                       <div class="row coopring">
                           <div class="col-md-8">
                               <p>&copy; 2015 NSFC . All Rights Reserved.</p>
                           </div>
                       </div>    
                       <!-- End coopring-->  

                    </div>
                    <!-- End Nav-->

                    <!-- Social-->
                    <div class="col-md-4">
                        <!-- Menu-->
                        <ul class="social">
                            <li data-toggle="tooltip" title data-original-title="Facebook">
                                <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Twitter">
                                <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li> 
                            <li data-toggle="tooltip" title data-original-title="Youtube">
                                <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>                     
                        </ul>
                        <!-- End Menu-->
                    </div>
                    <!-- End Social-->

               </div> 
                    
            </div>
        </footer>      
        <!-- End footer bottom-->

    </div>
    <!-- End layout-->

   
    <!-- End layout-->

    <!-- ======================= JQuery libs =========================== -->
    <!-- Always latest version of jQuery-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <!-- jQuery local-->    
    <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>    
    <!--Nav-->
    <script type="text/javascript" src="js/nav/tinynav.js"></script> 
    <script type="text/javascript" src="js/nav/superfish.js"></script>  
    <script type="text/javascript" src="js/nav/hoverIntent.js"></script> 
    <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>                                           
    <!--Totop-->
    <script type="text/javascript" src="js/totop/jquery.ui.totop.js" ></script>  
    <!--Slide-->
    <script type="text/javascript" src="js/slide/camera.js" ></script>      
    <script type='text/javascript' src='js/slide/jquery.easing.1.3.min.js'></script>  
    <!--FlexSlider-->
    <script src="js/flexslider/jquery.flexslider.js"></script> 
    <!--Ligbox--> 
    <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script> 
    <!-- carousel.js-->
    <script src="js/carousel/carousel.js"></script>  
    <!-- Twitter Feed-->
    <script src="js/twitter/jquery.tweet.js"></script> 
    <!-- flickr Feed-->
    <script src="js/flickr/jflickrfeed.min.js"></script>  
    <!--Scroll-->   
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Nicescroll -->
    <script src="js/scrollbar/jquery.nicescroll.js"></script>
    <!-- Maps -->
    <script src="js/maps/gmap3.js"></script>
    <!-- Filter -->
    <script src="js/filters/jquery.isotope.js" type="text/javascript"></script>
    <!--Theme Options-->
    <script type="text/javascript" src="js/theme-options/theme-options.js"></script>
    <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>                                
    <!-- Bootstrap.js-->
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <!--MAIN FUNCTIONS-->
    <script type="text/javascript" src="js/main.js"></script>
    <!-- ======================= End JQuery libs =========================== -->
        
    </body>
</html>